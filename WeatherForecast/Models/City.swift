//
//  City.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/6/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation

struct City {
    var name: String
    var id: Int 
}
