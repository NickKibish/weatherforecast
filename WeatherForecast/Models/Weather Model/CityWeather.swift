//
//  CityWeather.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/6/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation
import Unbox

struct CityWeather {
    var date: Date
    var pressure: Float
    var humidity: Int
    var speed: Float
    var degree: Int
    
    var weather: [Weather]
    var temperature: Temperature
}

extension CityWeather: Unboxable {
    init(unboxer: Unboxer) throws {
        self.pressure = try unboxer.unbox(key: "pressure")
        self.humidity = try unboxer.unbox(key: "humidity")
        self.speed = try unboxer.unbox(key: "speed")
        self.degree = try unboxer.unbox(key: "deg")
        self.date = Date()
        
        self.weather = try unboxer.unbox(key: "weather")
        self.temperature = try unboxer.unbox(key: "temp")
    }
}
