//
//  Temperature.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/6/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation
import Unbox

struct Temperature {
    var min: Float
    var max: Float
    
    var night: Float
    var morning: Float
    var day: Float
    var evening: Float
}

extension Temperature: Unboxable {
    init(unboxer: Unboxer) throws {
        self.min = try unboxer.unbox(key: "min")
        self.max = try unboxer.unbox(key: "max")
        
        self.night = try unboxer.unbox(key: "night")
        self.morning = try unboxer.unbox(key: "morn")
        self.day = try unboxer.unbox(key: "day")
        self.evening = try unboxer.unbox(key: "eve")
    }
}
