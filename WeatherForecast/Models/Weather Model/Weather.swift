//
//  Weather.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/6/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation
import Unbox

struct Weather {
    var id: Int
    var main: String
    var description: String
    var icon: String
}

extension Weather: Unboxable {
    init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "id")
        self.main = try unboxer.unbox(key: "main")
        self.description = try unboxer.unbox(key: "description")
        self.icon = try unboxer.unbox(key: "icon")
    }
}
