//
//  APIManager.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/7/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import Unbox

/// Errors
///
/// - paramsParseFailed: occures during wrong JSON
enum APIManagerError: Error {
    case paramsParseFailed
    
    /// Use it to get Error description
    ///
    /// - Returns: Human description
    func errorDescription() -> String {
        switch self {
        case .paramsParseFailed:
            return "Error occured during parameter parsing"
        }
    }
}

/// Typealias for Response completion
typealias WeatherCompletion = ((WeatherResponse) -> ())
/// Response type
///
/// - success: returns weather for tomorrow
/// - failure: returns error
enum WeatherResponse {
    case success(CityWeather)
    case failure(Error)
}

/// Use it fo make requests to server
class APIManager {
    private let baseURL = "http://api.openweathermap.org/data/2.5/forecast/daily"
    private let apiKey = "edc60874e635ded94b5ea2f4101774bc"
    
    private init() {
        
    }
    
    /// Singletone
    static let shared = APIManager()
    
    /// Use it to get weather for provided city
    ///
    /// - Parameters:
    ///   - city: provided city
    ///   - completion: response from server
    func getWeather(for city: City, completion: @escaping WeatherCompletion) {
        let params = ["id": city.id,
                          "cnt":2,
                          "APPID": apiKey] as [String : Any]
        
        Alamofire.request(baseURL, method: HTTPMethod.get, parameters: params).validate(statusCode: 200..<300).responseData { (dataResponse) in
            
            
            switch dataResponse.result {
            case .success(let data):
                
                guard let dict = JSON(data: data).dictionary?["list"]?.arrayObject?.first as? [String: Any] else {
                    completion(.failure(APIManagerError.paramsParseFailed))
                    return
                }
                
                do {
                    let weather: CityWeather = try unbox(dictionary: dict)
                    completion(.success(weather))
                } catch _ {
                    completion(.failure(APIManagerError.paramsParseFailed))
                }
                
                break
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
}
