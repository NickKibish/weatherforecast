//
//  WeatherDetailsViewController.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/7/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import UIKit
import SVProgressHUD

class WeatherDetailsViewController: UITableViewController {
    var viewModel: WeatherDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = viewModel.title
        reloadData()
    }
}

extension WeatherDetailsViewController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.detailTextLabel?.text = viewModel.detailText(for: indexPath)
    }
}

extension WeatherDetailsViewController {
    func reloadData() {
        SVProgressHUD.show()
        viewModel.reloadData { (response) in
            SVProgressHUD.dismiss()
            switch response {
            case .success(_):
                self.tableView.reloadData()
                break
            case .failure(let error):
                var message = "Something went wrong"
                if let apiError = error as? APIManagerError {
                    message = apiError.errorDescription()
                }
                self.showAlert(title: "Error", message: message)
                return
            }
        }
    }
}
