//
//  WeatherDetailsViewModel.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/7/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation
import Darwin

/// Parts of days
///
/// - night:
/// - morning:
/// - day:
/// - evening:
enum WeatherDetailsDayPart: Int {
    case night, morning, day, evening
}

class WeatherDetailsViewModel {
    /// Initial city
    var city: City
    /// Weather for initial city
    var weatherDetails: CityWeather?
    
    /// Returns title of view controller
    var title: String {
        let calendar = Calendar.current
        var components = calendar.dateComponents([.year, .month, .day], from: Date())
        if components.day != nil {
            (components.day)! += 1
        }
        
        guard let tomorrow = calendar.date(from: components) else { return city.name }
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        return "\(city.name) \(dateFormatter.string(from: tomorrow))"
    }
    
    /// Text for details labels in table view
    ///
    /// - Parameter indexPath: indexPath of cell
    /// - Returns: title for detailed label
    func detailText(for indexPath: IndexPath) -> String {
        switch indexPath.section {
        case 0:
            guard let dayPart = WeatherDetailsDayPart(rawValue: indexPath.row) else { return "" }
            return temperaturnForDayPart(dayPart: dayPart)
        case 1:
            return weatherForCell(at: indexPath.row)
        default:
            return "!"
        }
    }
    
    /// Main constructor
    ///
    /// - Parameter city: Initial city
    init(city: City) {
        self.city = city
    }
    
    /// Use it to get data from server
    ///
    /// - Parameter completion: completion closure
    func reloadData(completion: @escaping WeatherCompletion) {
        APIManager.shared.getWeather(for: city) { (response) in
            switch response {
            case .success(let weather):
                self.weatherDetails = weather
            default:
                break
            }
            DispatchQueue.main.async {
                completion(response)
            }
        }
    }
}

extension WeatherDetailsViewModel {
    fileprivate func weatherForCell(at index: Int) -> String {
        guard let details = weatherDetails else { return "" }
        switch index {
        case 0:
            return "\(details.pressure) hPa"
        case 1:
            return "\(details.humidity) %"
        case 2:
            return "\(details.speed) m/s, \(details.degree)° \(windDirectionToString(degree: details.degree))"
        case 3:
            return details.weather.map({ (weather) -> String in
                return weather.main
            }).joined(separator: ", ")
        default:
            return ""
        }
    }
    
    fileprivate func windDirectionToString(degree: Int) -> String {
        let dirictions = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "WNW", "NW", "NNW"]
        
        let segment = 360.0 / Float(dirictions.count)
        let index = Int((Float(degree) + segment / 2) / segment)
        return dirictions[index]
    }
    
    fileprivate func temperaturnForDayPart(dayPart: WeatherDetailsDayPart) -> String {
        switch dayPart {
        case .night: return temperatureTitle(for: weatherDetails?.temperature.night) ?? ""
        case .morning: return temperatureTitle(for: weatherDetails?.temperature.morning) ?? ""
        case .day: return temperatureTitle(for: weatherDetails?.temperature.day) ?? ""
        case .evening: return temperatureTitle(for: weatherDetails?.temperature.evening) ?? ""
        }
    }
    
    fileprivate func temperatureTitle(for temperature: Float?) -> String? {
        guard let kelvin = temperature else { return nil }
        let celcius = kelvin - 273
        return "\(Int(round(celcius))) C°"
    }
}
