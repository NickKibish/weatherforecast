//
//  CityListViewModel.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/7/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import Foundation

/// Errors, occured in CityListViewModel
///
/// - indexOutOfRange: throws if index out of range
enum CityListError: Error {
    case indexOutOfRange
    
    /// Use it to get Error description
    ///
    /// - Returns: Humanity Error Description
    func errorDescription() -> String {
        return "Index is out of range"
    }
}

class CityListViewModel {
    /// List of cities
    var cities: [City] = [City(name: "Lviv", id: 702550),
                          City(name: "Kiev", id: 703448),
                          City(name: "Vinnytsya", id: 689559),
                          City(name: "Kryvyy Rih", id: 703845)]
    
    /// Numbers of rows in table view
    var numberOfRows: Int {
        return cities.count
    }
    
    /// Name of the city
    ///
    /// - Parameter index: index of the city
    /// - Returns: Name of city
    /// - Throws: CityListError.indexOutOfRange if index is bigger than cities count
    func name(at index: Int) throws -> String {
        if index >= cities.count {
            throw CityListError.indexOutOfRange
        }
        
        return cities[index].name
    }
    
    /// Return view model for weather details of selected city
    ///
    /// - Parameter index: index of city
    /// - Returns: View Model for weather detail
    /// - Throws: CityListError.indexOutOfRange if index is bigger than cities count
    func weatherDetailViewModel(at index: Int) throws -> WeatherDetailsViewModel {
        if index >= cities.count {
            throw CityListError.indexOutOfRange
        }
        return WeatherDetailsViewModel(city: cities[index])
    }
}
