//
//  CityListTableViewController.swift
//  WeatherForecast
//
//  Created by Nick Kibish on 2/7/17.
//  Copyright © 2017 Nick Kibish. All rights reserved.
//

import UIKit

class CityListTableViewController: UITableViewController {
    var viewModel: CityListViewModel! {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? WeatherDetailsViewController,
            let index = tableView.indexPathForSelectedRow?.row else {
                return
        }
        do {
            vc.viewModel = try viewModel.weatherDetailViewModel(at: index)
        } catch let error {
            print(error)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else { return UITableViewCell() }
        do {
            cell.textLabel?.text = try viewModel.name(at: indexPath.row)
        } catch let error {
            print(error)
        }
        return cell
    }
}
